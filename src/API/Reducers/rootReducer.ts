import { combineReducers } from "@reduxjs/toolkit";
import authReducer from "./AuthReducer";

const rootReducer = combineReducers({
    user: authReducer,
});

export default rootReducer;