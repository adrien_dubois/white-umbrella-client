import ReactDOM from 'react-dom/client';
import App from 'App';
import { ModalContextProvider } from 'utils/hooks/Modal/ModalContext';
import { ToastContextProvider } from 'utils/hooks/Toasts/ToastContext';
import { PreloadedState, configureStore } from '@reduxjs/toolkit';
import rootReducer from 'API/Reducers/rootReducer';
import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

export const makeStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false
    }),
    devTools: true,
    preloadedState
  })
}

const store = makeStore();

root.render(
  <>
  <ModalContextProvider>
    <ToastContextProvider>
      <App />
    </ToastContextProvider>
  </ModalContextProvider>
  </>
);

export default store;
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
export type AppStore = ReturnType<typeof makeStore>;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useAppDispatch = () => useDispatch<AppDispatch>();

