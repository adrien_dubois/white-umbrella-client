// TYPES

export type AuthT = {
    token: string,
    data: UserT,
}

export type UserT = {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    roles: string[],
}