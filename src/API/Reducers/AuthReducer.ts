import { CLEAR_AUTH, SET_AUTH } from "API/GlobalTypes";
import { AuthT } from "utils/types";

const initialState: AuthT = {
    token: '',
    data: {
        id: 0,
        firstName: '',
        lastName: '',
        email: '',
        roles: [],
    },
}

const authReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_AUTH:
            return {
                ...state,
                token: action.payload.token,
                data: action.payload.data,
            }
        case CLEAR_AUTH:
            return {
                ...state,
                token: '',
                data: {
                    id: 0,
                    firstName: '',
                    lastName: '',
                    email: '',
                    roles: [],
                },
            }
        default:
            return state;
    }
}

export default authReducer;