import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
:root{
        /*----- FONTS -----*/
        --roboto: 'Roboto', sans-serif;
        --barlow: 'Barlow', sans-serif;
        --rubik : 'Rubik',  sans-serif;

        /*----- COLORS -----*/
        --black         : #242d49;
        --white         : #FAECD9;
        --yellow        : #f5c32c;
        --orange        : #fca61f;
        --gray          : rgba(36, 45, 73, 0.65);
        --profileShadow : 0px 4px 17px 2px rgba(0, 0, 0, 0.25);
        --darkBg        : #0F0E13;
        --hrColor       : #cfcdcd;
        --hrBlack       : #4d4d4e ;
        --cardColor     : rgba(255, 255, 255, 0.64);
        --cardColorBlack: rgba(0, 0, 0, 0.205);
        --error-color   : #F43B47;
        /*----- GRADIENT ------*/
        --dark-welcome: radial-gradient(at 0% 0%, hsla(253,16%,7%,1) 0, transparent 50%), radial-gradient(at 50% 0%, hsla(225,39%,30%,1) 0, transparent 50%), radial-gradient(at 100% 0%, hsla(339,49%,30%,1) 0, transparent 50%);
    }

        * {
            margin         : 0;
            padding        : 0;
            list-style-type: none;
            list-style     : none;
            outline        : none;
            border         : none;
            text-decoration: none;
        }
        html {
            overscroll-behavior: contain;
            overflow           : hidden;
            box-sizing         : border-box;
            height             : 100%;
            font-size          : 16px;
            @media (prefers-reduced-motion: no-preference) {
                scroll-behavior: smooth;
            }
        }

        body{
            margin                 : 0;
            height: 100%;
            padding                : 0;
            line-height            : 1;
            font-smooth            : always;
            font-family            : var(--roboto);
            -webkit-font-smoothing : antialiased;
            -moz-osx-font-smoothing: grayscale;
            overflow               : hidden;
            display                : block;
            
            @media screen and (max-width: 1200px){
                overflow: visible;
            }
        }

        textarea, input{
            font-family: var(--ubuntu-font);
        }

        .toast-container{
            position: fixed;
            right   : 20px;
            top     : 20px;
            z-index : 999;
        }
`;

export default GlobalStyle;

export const lightTheme = {

};

export const darkTheme = {

};